FROM node:16 AS builder
COPY ./ /landing-page
WORKDIR /landing-page
RUN yarn install && yarn next build && yarn next export -o _build


FROM nginx:stable-alpine
ENV DEBIAN_FRONTEND noninteractive
COPY --from=builder /landing-page/_build/ /usr/share/nginx/html/

# docker build -t landing-page .
# docker run --rm -ti -p 80:80 landing-page