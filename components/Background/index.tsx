import Wallpaper from '../../public/fly-away.webp'
import { Grid } from '@mui/material'

const gridStyles = {
  position: 'absolute',
  top: 0,
  left: 0,
  zIndex: -1,
  opacity: 1,
  width: '100%',
  height: '100%',
  backgroundSize: 'cover',
  backgroundPosition: 'center',
  backgroundImage: `linear-gradient(rgba(255, 255, 255, 0), #06043E), url('${Wallpaper.src}')`
}

const Background = () => {
  return (
    <>
      <Grid
        sx={gridStyles}
        container
      />
    </>
  )
}

export default Background