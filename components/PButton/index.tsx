import { Button, ButtonProps } from '@mui/material'
import Link from 'next/link'
import theme from '../Theme'

import gradientOffset from '../../lib/gradient'

const styles = {
  root: {
    borderRadius: 85,
    minWidth: 165,
    textTransform: 'none',
    fontSize: 14,
    fontWeight: 700,
    marginTop: theme.spacing(4),
    paddingTop: theme.spacing(1.5),
    paddingBottom: theme.spacing(1.5),
    [theme.breakpoints.down('sm')]: {
      fontSize: 12
    }
  },
  contained: {
    color: '#03232E',
    background: '#03232E'
  },
  gradient: {
    background: `linear-gradient(99.16deg, ${theme.palette.primary.main} 24.01%, ${gradientOffset(
      theme.palette.primary.main
    )} 81.75%)`,
    transition: 'box-shadow 0.3s ease-in-out',
    '&:hover': {
      boxShadow: '0 0 30px 10px #2c2981'
    },
    color: '#03232E'
  }
}

type PButtonProps = {
  to: string
  children: string
} & Omit<ButtonProps, 'children'>

const PButton = ({
   children,
   startIcon,
   to,
   style,
   size,
   ...rest
 }: PButtonProps) => {
  return (
    <Link href={to}>
      <Button
        sx={[styles.root, styles.contained, styles.gradient]}
        startIcon={startIcon}
        style={style}
        size={size}
        disableElevation
        {...rest}
      >
        {children}
      </Button>
    </Link>
  )
}

export default PButton