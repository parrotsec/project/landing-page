import { createTheme } from '@mui/material'

const theme = createTheme({
  breakpoints: {
    values: {
      xs: 0,
      sm: 600,
      md: 960,
      lg: 1280,
      xl: 1920
    }
  },
  palette: {
    primary: {
      main: '#05EEFF'
    },
    background: {
      default: '#06043E',
      paper: '#19174B'
    }
  },
  typography: {
    fontFamily: 'museo-sans',
    fontSize: 16,
    h1: {
      fontSize: 72, //theme.spacing(9)
      fontWeight: 900
    },
    h2: {
      fontSize: 64, //theme.spacing(8)
      fontWeight: 900
    },
    h3: {
      fontSize: 48, //theme.spacing(6)
      fontWeight: 900
    },
    h4: {
      fontSize: 40, //theme.spacing(5)
      fontWeight: 900
    },
    h5: {
      fontSize: 28, //theme.spacing(3.5)
      fontWeight: 900
    },
    h6: {
      fontSize: 24, //theme.spacing(3)
      fontWeight: 900
    },
    body1: {
      fontWeight: 'normal',
      fontSize: 16,
      fontFamily: 'nimbus-sans'
    },
    body2: {
      fontWeight: 'normal',
      fontSize: 14,
      fontFamily: 'nimbus-sans'
    },
    subtitle1: {
      fontSize: 20,
      fontWeight: 400,
      fontFamily: 'nimbus-sans'
    },
    subtitle2: {
      fontSize: 18,
      fontFamily: 'nimbus-sans'
    }
  },
  components: {
    MuiPaper: {
      styleOverrides: {
        rounded: {
          borderRadius: 24
        }
      }
    },
    MuiTypography: {
      styleOverrides: {
        paragraph: {
          marginBottom: 20
        }
      }
    },
    MuiButtonBase: {
      styleOverrides: {
        root: {
          fontWeight: 700
        }
      }
    },
    MuiLink: {
      styleOverrides: {
        root: {
          fontWeight: 400
        }
      }
    }
  }
})

export default theme