import type { NextPage } from 'next'
import { Grid } from '@mui/material'
import PButton from '../components/PButton'
import socials from '../components/social'
import Image from 'next/image'
import ParrotLogo from '../public/parrot-logo.png'

const socialData = socials.map((data, i) => (
    <PButton key={`id-${i}`} size='medium' to={data.link}>{data.name}</PButton>
))

const Home: NextPage = () => {
  return (
      <Grid
        sx={{ paddingTop: 5 }}
        container
        justifyContent='center'
        alignItems='center'
        direction='column'
        wrap='nowrap'
      >
        <Image src={ParrotLogo} width={130} height={120} alt="Parrot logo" />
        {socialData}
      </Grid>
  )
}

export default Home
